import samples from '../../data/samples.js';

const state = {
    samples: []
};

const mutations = {
    'SET_SAMPLES' (state, samples) {
        state.samples = samples;
    },
    'PUSH_SAMPLE' (state, {id, contNum, temp, cL, turb, pH, fixture, isColiformSample, addrId}) {
        //set sample ID to always be 1 more than the length of the array
        id = state.samples.length +1;
        state.samples.push({
            id: id,
            contNum: contNum,
            temp: temp,
            cL: cL,
            turb: turb,
            pH: pH,
            fixture: fixture,
            isColiformSample: isColiformSample,
            addrId: addrId
        });
    }
};

const actions = {
    initSamples: ({commit}) => {
        commit('SET_SAMPLES', samples);
    },
    addSample({commit}, sample) {
        commit('PUSH_SAMPLE', sample);
    }
};

const getters = {
    
};

export default {
    state,
    mutations,
    actions,
    getters
};