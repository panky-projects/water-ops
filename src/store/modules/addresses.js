import addresses from '../../data/addresses.js';

const state = {
    addresses: []
};

const mutations = {
    'SET_ADDRESSES' (state, addresses) {
        state.addresses = addresses;
    }
};

const actions = {
    //set initial array of addresses
    initAddresses: ({commit}) => {
        commit('SET_ADDRESSES', addresses);
    }
};

const getters = {
    getAddresses: state => {
        return state.addresses;
    },
    getAddrId: state => {
        return state.addresses.addrId;
    }
};

export default {
    state,
    mutations,
    actions,
    getters
};