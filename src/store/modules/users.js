import users from '../../data/users.js';

const state = {
    users: []
};

const mutations = {
    'SET_USERS' (state, users) {
        state.users = users;
    },
    //check to see if the password/username combo matches with the users array
    'LOGIN' (state, {name, pass}) {
        const recordName = state.users.find(element => element.username == name);
        const recordPass = state.stocks.find(element => element.password == pass);
        if (recordName & recordPass) {
            return true;
        } else {
            return false;
        }
    }
};

const actions = {
    //set initial array of users
    initUsers: ({commit}) => {
        commit('SET_USERS', users);
    },
    login: ({commit}, token) => {
        commit('LOGIN', token);
    }
};

const getters = {
    users: state => {
        return state.users;
    }
};

export default {
    state,
    mutations,
    actions,
    getters
};