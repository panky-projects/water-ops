import containers from '../../data/containers.js';

const state = {
    containers: []
};

const mutations = {
    'SET_CONTAINERS' (state, containers) {
        state.containers = containers;
    }
};

const actions = {
    initContainers: ({commit}) => {
        commit('SET_CONTAINERS', containers);
    }
};

const getters = {
    getContainers: state => {
        return state.containers;
    }
}

export default {
    state,
    mutations,
    actions,
    getters
};