import Vue from 'vue';
import Vuex from 'vuex';

//import modules
import users from './modules/users.js';
import addresses from './modules/addresses.js';
import samples from './modules/samples.js';
import containers from './modules/containers.js';

Vue.use(Vuex);

export default new Vuex.Store ({
    modules: {
        users,
        addresses,
        samples,
        containers
    }
});