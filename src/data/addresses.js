export default [
    {
        addrNum: 112,
        street: 'Abe Str',
        suite: '', 
        city: 'Lincoln', 
        state: 'MD',
        zipcode: 20902,
        country: 'United States',
        addrId: 1
    },
    { 
        addrNum: 606,
        street: 'Milky Rd',
        suite: '', 
        city: 'Lincoln', 
        state: 'MD',
        zipcode: 20902,
        country: 'United States',
        addrId: 2
    },
    {
        addrNum: 444, 
        street: 'Dunder Dr',
        suite: '14B', 
        city: 'Mythland', 
        state: 'MD',
        zipcode: 20583,
        country: 'United States',
        addrId: 3
    },
    {
        addrNum: 624, 
        street: 'Johnsons Rd', 
        suite: '2A',
        city: 'Rocksalot', 
        state: 'MD',
        zipcode: 20901,
        country: 'United States',
        addrId: 4
    },
    {
        addrNum: 501, 
        street: 'Abbey Str',
        suite: '', 
        city: 'Salty Springs', 
        state: 'MD',
        zipcode: 20854,
        country: 'United States',
        addrId: 5
    },
];