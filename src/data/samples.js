export default [ 
    {
        id: 1,
        contNum: 1001,
        temp: 82,
        cL: 12,
        turb: 22,
        pH: 7.5,
        isColiformSample: true,
        fixture: 'Powder Room',
        addrId: 1
    },
    {
        id: 2,
        contNum: 1002,
        temp: 80,
        cL: 10,
        turb: 20,
        pH: 7.0,
        isColiformSample: false,
        fixture: 'Downstair Bathroom',
        addrId: 1
    }
];