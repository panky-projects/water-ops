export default [
    {
        id: 1,
        username: 'abelin1',
        password: '123456',
        firstName: 'Abe', 
        lastName: 'Lincoln', 
        role: 'Admin'
    },
    {
        id: 2, 
        userame: 'babeljohn2',
        password: '123456',
        firstName: 'Babel', 
        lastName: 'Johnson',
        role: 'Lab'
    },
    {
        id: 3, 
        username: 'christylin3',
        password: '123456',
        firstName: 'Christy', 
        lastName: 'Lincoln', 
        role: 'Sampler'
    },
    {
        id: 4,
        username: 'dickpen4',
        password: '123456',
        firstName: 'Dick', 
        lastName: 'Penson', 
        role: 'Manager'
    },
    {
        id: 5, 
        username: 'emilyrob5',
        password: '123456',
        firstName: 'Emily', 
        lastName: 'Robinson', 
        role: 'Sampler'
    },
    {
        id: 6, 
        username: 'freddoug6',
        password: '123456',
        firstName: 'Fred', 
        lastName: 'Douglas',
        role: 'Lab'
        
    }
];