import Vue from 'vue'
import Router from 'vue-router'

//import Login from './components/Login.vue';
import Home from '@/components/Home';
//admin components
import Account from '@/components/admin/Account';
import AdminDash from '@/components/admin/Dashboard';
import System from '@/components/admin/System';
import Users from '@/components/admin/Users';
//sampler components
import SamplerDash from '@/components/sampler/Dashboard';
import Calendar from '@/components/sampler/Calendar';
import Completed from '@/components/sampler/ClosedTasks';
import Containers from '@/components/sampler/Containers';
import Current from '@/components/sampler/OpenTasks';
import Map from '@/components/sampler/Map';

Vue.use(Router)

export default new Router({
  routes: [
    //{ path: '', name: "login", component: Login },
    { path: '/', name: 'index', component: Home },
    { path: '/home', name: 'home', component: Home },
    //admin paths
    { path: '/admin/account', name: 'adminAccount', component: Account },
    { path: '/admin/dash', name: 'adminDash', component: AdminDash },
    { path: '/admin/system', name: 'system', component: System },
    { path: '/admin/users', name: 'users', component: Users },
    //sampler paths
    { path: '/sampler/dash', name: 'samplerDash', component: SamplerDash },
    { path: '/sampler/calendar', name: 'samplerCalendar', component: Calendar },
    { path: '/sampler/completed', name: 'closedTasks', component: Completed },
    { path: '/sampler/containers', name: 'containers', component: Containers },
    { path: '/sampler/current', name: 'openTasks', component: Current },
    { path: '/sampler/map', name: 'samplerMap', component: Map }
  ]
})
